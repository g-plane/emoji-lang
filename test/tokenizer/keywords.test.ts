import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'
import { TokenType, Emojis } from '../../dist/tokenizer/types'

test('if', t => {
  const tokens = tokenizer('❓')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.IfKeyword)
  t.is(tokens[0].value, Emojis.If)
})

test('else', t => {
  const tokens = tokenizer('❗️')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.ElseKeyword)
  t.is(tokens[0].value, Emojis.Else)
})
