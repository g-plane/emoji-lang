import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'
import { TokenType, Emojis } from '../../dist/tokenizer/types'

test('true', t => {
  const tokens = tokenizer('✅')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.BooleanLiteral)
  t.is(tokens[0].value, Emojis.True)
})

test('false', t => {
  const tokens = tokenizer('❌')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.BooleanLiteral)
  t.is(tokens[0].value, Emojis.False)
})
