import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'
import { TokenType, Emojis } from '../../dist/tokenizer/types'

test('assign with normal character', t => {
  const tokens = tokenizer('a = 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Assign)
  t.is(tokens[1].value, '=')
})

test('assign with emoji', t => {
  const tokens = tokenizer('a 🚚 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Assign)
  t.is(tokens[1].value, Emojis.Assign)
})

test('dot', t => {
  const tokens = tokenizer('.')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.Dot)
  t.is(tokens[0].value, '.')
})

test('paren start', t => {
  const tokens = tokenizer('(')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.ParenStart)
  t.is(tokens[0].value, '(')
})

test('paren end', t => {
  const tokens = tokenizer(')')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.ParenEnd)
  t.is(tokens[0].value, ')')
})

test('comma', t => {
  const tokens = tokenizer(',')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.Comma)
  t.is(tokens[0].value, ',')
})

test('curly brace start', t => {
  const tokens = tokenizer('{')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.CurlyBraceStart)
  t.is(tokens[0].value, '{')
})

test('curly brace end', t => {
  const tokens = tokenizer('}')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.CurlyBraceEnd)
  t.is(tokens[0].value, '}')
})

test('bracket start', t => {
  const tokens = tokenizer('[')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.BracketStart)
  t.is(tokens[0].value, '[')
})

test('bracket end', t => {
  const tokens = tokenizer(']')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.BracketEnd)
  t.is(tokens[0].value, ']')
})

test('colon', t => {
  const tokens = tokenizer(':')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.Colon)
  t.is(tokens[0].value, ':')
})

test('semicolon', t => {
  const tokens = tokenizer(';')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.Semicolon)
  t.is(tokens[0].value, ';')
})

test('add with normal character', t => {
  const tokens = tokenizer('a + 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Add)
  t.is(tokens[1].value, '+')
})

test('add with emoji', t => {
  const tokens = tokenizer('a ➕ 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Add)
  t.is(tokens[1].value, Emojis.Add)
})

test('substract with normal character', t => {
  const tokens = tokenizer('a - 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Substract)
  t.is(tokens[1].value, '-')
})

test('substract with emoji', t => {
  const tokens = tokenizer('a ➖ 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Substract)
  t.is(tokens[1].value, Emojis.Substract)
})

test('multiply with normal character', t => {
  const tokens = tokenizer('a * 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Multiply)
  t.is(tokens[1].value, '*')
})

test('multiply with emoji', t => {
  const tokens = tokenizer('a ✖️ 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Multiply)
  t.is(tokens[1].value, Emojis.Multiply)
})

test('divide with normal character', t => {
  const tokens = tokenizer('a / 1')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Divide)
  t.is(tokens[1].value, '/')
})

test('divide with emoji', t => {
  const tokens = tokenizer('a ➗ 1')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Divide)
  t.is(tokens[1].value, Emojis.Divide)
})

test('add assign with normal character', t => {
  const tokens = tokenizer('a += 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.AddAssign)
  t.is(tokens[1].value, '+=')
})

test('add assign with emoji', t => {
  const tokens = tokenizer('a ➕🚚 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.AddAssign)
  t.is(tokens[1].value, Emojis.Add + Emojis.Assign)
})

test('substract assign with normal character', t => {
  const tokens = tokenizer('a -= 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.SubstractAssign)
  t.is(tokens[1].value, '-=')
})

test('substract assign with emoji', t => {
  const tokens = tokenizer('a ➖🚚 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.SubstractAssign)
  t.is(tokens[1].value, Emojis.Substract + Emojis.Assign)
})

test('multiply assign with normal character', t => {
  const tokens = tokenizer('a *= 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.MultiplyAssign)
  t.is(tokens[1].value, '*=')
})

test('multiply assign with emoji', t => {
  const tokens = tokenizer('a ✖️🚚 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.MultiplyAssign)
  t.is(tokens[1].value, Emojis.Multiply + Emojis.Assign)
})

test('divide assign with normal character', t => {
  const tokens = tokenizer('a /= 1')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.DivideAssign)
  t.is(tokens[1].value, '/=')
})

test('divide assign with emoji', t => {
  const tokens = tokenizer('a ➗🚚 1')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.DivideAssign)
  t.is(tokens[1].value, Emojis.Divide + Emojis.Assign)
})

test('equal with normal character', t => {
  const tokens = tokenizer('a == 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Equal)
  t.is(tokens[1].value, '==')
})

test('equal with emoji', t => {
  const tokens = tokenizer('a 😻 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.Equal)
  t.is(tokens[1].value, Emojis.Equal)
})

test('not equal with normal character', t => {
  const tokens = tokenizer('a != 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.NotEqual)
  t.is(tokens[1].value, '!=')
})

test('not equal with emoji', t => {
  const tokens = tokenizer('a 🙀 0')
  t.is(tokens.length, 3)
  t.is(tokens[1].type, TokenType.NotEqual)
  t.is(tokens[1].value, Emojis.NotEqual)
})

test('negate with normal character', t => {
  const tokens = tokenizer('!a')
  t.is(tokens.length, 2)
  t.is(tokens[0].type, TokenType.Negate)
  t.is(tokens[0].value, '!')
})

test('negate with emoji', t => {
  const tokens = tokenizer('✋a')
  t.is(tokens.length, 2)
  t.is(tokens[0].type, TokenType.Negate)
  t.is(tokens[0].value, Emojis.Negate)
})

test('increment with normal character', t => {
  const tokens = tokenizer('a++')
  t.is(tokens.length, 2)
  t.is(tokens[1].type, TokenType.Increment)
  t.is(tokens[1].value, '++')
})

test('increment with emoji', t => {
  const tokens = tokenizer('a📈')
  t.is(tokens.length, 2)
  t.is(tokens[1].type, TokenType.Increment)
  t.is(tokens[1].value, Emojis.Increment)
})

test('decrement with normal character', t => {
  const tokens = tokenizer('a--')
  t.is(tokens.length, 2)
  t.is(tokens[1].type, TokenType.Decrement)
  t.is(tokens[1].value, '--')
})

test('decrement with emoji', t => {
  const tokens = tokenizer('a📉')
  t.is(tokens.length, 2)
  t.is(tokens[1].type, TokenType.Decrement)
  t.is(tokens[1].value, Emojis.Decrement)
})
