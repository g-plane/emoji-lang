import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'
import { TokenType } from '../../dist/tokenizer/types'

test('ignored linebreaks', t => {
  t.is(tokenizer('(a,\nb)').length, 5)
  t.is(tokenizer('a.\nb').length, 3)
  t.is(tokenizer('(\na)').length, 3)
  t.is(tokenizer('[\na]').length, 3)
  t.is(tokenizer('{\na}').length, 3)
})

test('preserved linebreaks', t => {
  t.is(tokenizer('a\nb').length, 3)
  t.is(tokenizer('a\r\nb').length, 3)
})
