import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'
import { TokenType } from '../../dist/tokenizer/types'

test('empty source code', t => {
  t.is(tokenizer('').length, 0)
})

test('space', t => {
  t.is(tokenizer('" " " "').length, 6)
})

test('strip leading linebreaks and spaces', t => {
  // linebreaks
  t.is(tokenizer('\na').length, 1)
  t.is(tokenizer('\n\na').length, 1)
  t.is(tokenizer('\r\na').length, 1)
  t.is(tokenizer('\r\n\r\na').length, 1)
  t.is(tokenizer('\r\n\ra').length, 1)
  t.is(tokenizer('\n\r\na').length, 1)

  // spaces
  t.is(tokenizer(' a').length, 1)
  t.is(tokenizer('  a').length, 1)

  // mix
  t.is(tokenizer('  \ra').length, 1)
  t.is(tokenizer('  \na').length, 1)
  t.is(tokenizer('\n a').length, 1)
  t.is(tokenizer('\r\n a').length, 1)
})

test('strip trailing linebreaks and spaces', t => {
  // linebreaks
  t.is(tokenizer('a\n').length, 1)
  t.is(tokenizer('a\n\n').length, 1)
  t.is(tokenizer('a\r\n').length, 1)
  t.is(tokenizer('a\r\n\r\n').length, 1)
  t.is(tokenizer('a\r\n\r').length, 1)
  t.is(tokenizer('a\n\r\n').length, 1)

  // spaces
  t.is(tokenizer('a ').length, 1)
  t.is(tokenizer('a  ').length, 1)

  // mix
  t.is(tokenizer('a  \r').length, 1)
  t.is(tokenizer('a  \n').length, 1)
  t.is(tokenizer('a\n ').length, 1)
  t.is(tokenizer('a\r\n ').length, 1)
})
