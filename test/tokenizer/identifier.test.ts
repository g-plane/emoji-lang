import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'
import { TokenType } from '../../dist/tokenizer/types'

test('normal identifier', t => {
  const tokens = tokenizer('abc_123def')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.Identifier)
  t.is(tokens[0].value, 'abc_123def')

  t.is(tokenizer('_')[0].value, '_')
  t.is(tokenizer('_123')[0].value, '_123')
  t.is(tokenizer('CAPITAL')[0].value, 'CAPITAL')
})

test('invalid identifier', t => {
  t.is(tokenizer('1a').length, 2)
})

test('stdout identifier', t => {
  const tokens = tokenizer('📝')
  t.is(tokens[0].type, TokenType.Identifier)
  t.is(tokens[0].value, '📝')
})
