import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'

test('ignore comments with double slashes', t => {
  t.is(tokenizer('//').length, 0)
  t.is(tokenizer('//comment').length, 0)
  t.is(tokenizer('//comment\n').length, 0)
})

test('ignore comments with emoji', t => {
  t.is(tokenizer('💩').length, 0)
  t.is(tokenizer('💩comment').length, 0)
  t.is(tokenizer('💩comment\n').length, 0)
})
