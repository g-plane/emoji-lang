import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'
import { TokenType } from '../../dist/tokenizer/types'

test('pure digits', t => {
  const tokens = tokenizer('1234567890')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.NumberLiteral)
  t.is(tokens[0].value, '1234567890')
})

test('emojis', t => {
  const tokens = tokenizer('1️⃣2️⃣3️⃣4️⃣5️⃣6️⃣7️⃣8️⃣9️⃣0️⃣')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.NumberLiteral)
  t.is(tokens[0].value, '1234567890')
})

test('mix digits and emojis', t => {
  const tokens = tokenizer('1️⃣23️⃣45️⃣67️⃣89️⃣0')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.NumberLiteral)
  t.is(tokens[0].value, '1234567890')
})

test('number separator', t => {
  const tokens = tokenizer('1️⃣2_3️⃣45️⃣_67️⃣8_9️⃣0')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.NumberLiteral)
  t.is(tokens[0].value, '1234567890')

  t.is(tokenizer('0xa_b')[0].value, '0xab')

  t.throws(() => tokenizer('1_'))
})

test('float', t => {
  t.is(tokenizer('1.2')[0].value, '1.2')
  t.is(tokenizer('1.2️⃣')[0].value, '1.2')

  t.throws(() => tokenizer('12.3.4'))
})

test('valid hex', t => {
  const tokens = tokenizer('0️⃣x1️⃣2Ab')
  t.is(tokens.length, 1)
  t.is(tokens[0].type, TokenType.NumberLiteral)
  t.is(tokens[0].value, '0x12Ab')

  t.is(tokenizer('0x57cE')[0].value, '0x57cE')
})

test('invalid hex', t => {
  t.throws(() => tokenizer('1x1️⃣2Ab'))
  t.throws(() => tokenizer('00x1️⃣2Ab'))
  t.throws(() => tokenizer('0x'))
  t.throws(() => tokenizer('0xab.1'))
})
