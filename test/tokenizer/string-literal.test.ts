import test from 'ava'
import { tokenizer } from '../../dist/tokenizer'
import { TokenType } from '../../dist/tokenizer/types'

test('single quote', t => {
  const tokens = tokenizer("'hi\"\"``\\''")
  t.is(tokens.length, 3)
  t.is(tokens[0].type, TokenType.QuoteStart)
  t.is(tokens[0].value, '\'')
  t.is(tokens[1].type, TokenType.StringLiteral)
  t.is(tokens[1].value, 'hi""``\'')
  t.is(tokens[2].type, TokenType.QuoteEnd)
  t.is(tokens[2].value, '\'')
})

test('double quote', t => {
  const tokens = tokenizer('"hi``\'\'\\""')
  t.is(tokens.length, 3)
  t.is(tokens[0].type, TokenType.QuoteStart)
  t.is(tokens[0].value, '"')
  t.is(tokens[1].type, TokenType.StringLiteral)
  t.is(tokens[1].value, 'hi``\'\'"')
  t.is(tokens[2].type, TokenType.QuoteEnd)
  t.is(tokens[2].value, '"')
})

test('backtick', t => {
  const tokens = tokenizer('`hi""\'\'\\``')
  t.is(tokens.length, 3)
  t.is(tokens[0].type, TokenType.QuoteStart)
  t.is(tokens[0].value, '`')
  t.is(tokens[1].type, TokenType.StringLiteral)
  t.is(tokens[1].value, 'hi""\'\'`')
  t.is(tokens[2].type, TokenType.QuoteEnd)
  t.is(tokens[2].value, '`')
})

test('disallow linebreak in single quotes or double quotes', t => {
  t.throws(() => tokenizer("'a\nb'"), { instanceOf: SyntaxError })
  t.throws(() => tokenizer('"a\nb"'), { instanceOf: SyntaxError })
})

test('allow linebreak in backticks', t => {
  t.notThrows(() => tokenizer('`a\nb`'))
})

test('escape in single quotes', t => {
  const tokens = tokenizer("'\\n\\b'")
  t.is(tokens[1].value, '\\n\\b')
})

test('unneeded escape in single quotes', t => {
  const tokens = tokenizer("'\\a'")
  t.is(tokens[1].value, 'a')
})

test('escape in double quotes', t => {
  const tokens = tokenizer('"\\n\\b"')
  t.is(tokens[1].value, '\\n\\b')
})

test('unneeded escape in double quotes', t => {
  const tokens = tokenizer('"\\a"')
  t.is(tokens[1].value, 'a')
})

test('escape in backticks', t => {
  const tokens = tokenizer('`\\n\\b`')
  t.is(tokens[1].value, '\\n\\b')
})

test('unneeded escape in backticks', t => {
  const tokens = tokenizer('`\\a`')
  t.is(tokens[1].value, 'a')
})

test('error should be thrown if string is not finished', t => {
  t.throws(() => tokenizer('"'))
  t.throws(() => tokenizer('"""'))
  t.throws(() => tokenizer('\''))
  t.throws(() => tokenizer('\'\'\''))
  t.throws(() => tokenizer('`'))
  t.throws(() => tokenizer('```'))
})

test('empty string', t => {
  t.is(tokenizer("''").length, 2)
  t.is(tokenizer('""').length, 2)
  t.is(tokenizer('``').length, 2)
})
