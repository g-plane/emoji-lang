import { Token, TokenType, Emojis } from './types'
import { lastToken, throwTokenizerError, isMatchEmoji } from './utils'

const CAN_BE_ESCAPED = 'nb'
const NUMBER_REGEX = /\d/
const VALID_IDENTIFIER_FIRST_CHAR = /[a-zA-Z_]/
const VALID_IDENTIFIER = /\w/
const HEX_LETTER = /[a-fA-F]/

export function tokenizer(input: string): Token[] {
  input = input.replace(/^\s+/g, '').replace(/\s+$/g, '')

  const tokens: Token[] = []
  let cursor = 0
  const { length } = input

  function tokenizeString(quote: '\'' | '"' | '`'): void {
    const allowLinebreak = quote === '`'

    tokens.push({ type: TokenType.QuoteStart, value: input[cursor] })

    let string = ''
    cursor++

    while (cursor < length) {
      const current = input[cursor]
      const next = input[cursor + 1]

      if (!allowLinebreak && current === '\n') {
        throwTokenizerError(cursor)
      } else if (current === quote) {
        if (string) {
          tokens.push({ type: TokenType.StringLiteral, value: string })
        }
        tokens.push({ type: TokenType.QuoteEnd, value: current })
        cursor++
        break
      } else if (current === '\\') {
        if (CAN_BE_ESCAPED.includes(next)) {
          string += '\\' + next
          cursor += 2
        } else if (next === quote) {
          string += quote
          cursor += 2
        } else {
          cursor++
        }
      } else {
        string += current
        cursor++
      }
    }

    if (lastToken(tokens).type !== TokenType.QuoteEnd) {
      throwTokenizerError(cursor)
    }
  }

  while (cursor < length) {
    const current = input[cursor]
    const next = input[cursor + 1]

    if (isMatchEmoji(Emojis.Stdout, current, next)) {
      tokens.push({ type: TokenType.Identifier, value: Emojis.Stdout })
      cursor += 2
    } else if (isMatchEmoji(Emojis.Comment, current, next) || (current === '/' && next === '/')) {
      while (input[cursor] === '\n' || cursor < length) {
        cursor++
      }
    } else if (isMatchEmoji(Emojis.If, current, next)) {
      tokens.push({ type: TokenType.IfKeyword, value: Emojis.If })
      cursor += 1
    } else if (isMatchEmoji(Emojis.Else, current, next)) {
      tokens.push({ type: TokenType.ElseKeyword, value: Emojis.Else })
      cursor += 2
    } else if (isMatchEmoji(Emojis.True, current, next)) {
      tokens.push({ type: TokenType.BooleanLiteral, value: Emojis.True })
      cursor++
    } else if (isMatchEmoji(Emojis.False, current, next)) {
      tokens.push({ type: TokenType.BooleanLiteral, value: Emojis.False })
      cursor++
    } else if (isMatchEmoji(Emojis.Assign, current, next)) {
      tokens.push({ type: TokenType.Assign, value: Emojis.Assign })
      cursor += 2
    } else if (isMatchEmoji(Emojis.Negate, current, next)) {
      tokens.push({ type: TokenType.Negate, value: Emojis.Negate })
      cursor++
    } else if (isMatchEmoji(Emojis.Equal, current, next)) {
      tokens.push({ type: TokenType.Equal, value: Emojis.Equal })
      cursor += 2
    } else if (isMatchEmoji(Emojis.NotEqual, current, next)) {
      tokens.push({ type: TokenType.NotEqual, value: Emojis.NotEqual })
      cursor += 2
    } else if (isMatchEmoji(Emojis.Add, current, next)) {
      if (isMatchEmoji(Emojis.Assign, next, input[cursor + 2])) {
        tokens.push({ type: TokenType.AddAssign, value: Emojis.Add + Emojis.Assign })
        cursor += 3
      } else {
        tokens.push({ type: TokenType.Add, value: Emojis.Add })
        cursor += 1
      }
    } else if (isMatchEmoji(Emojis.Substract, current, next)) {
      if (isMatchEmoji(Emojis.Assign, next, input[cursor + 2])) {
        tokens.push({ type: TokenType.SubstractAssign, value: Emojis.Substract + Emojis.Assign })
        cursor += 3
      } else {
        tokens.push({ type: TokenType.Substract, value: Emojis.Substract })
        cursor += 1
      }
    } else if (isMatchEmoji(Emojis.Multiply, current, next)) {
      if (isMatchEmoji(Emojis.Assign, input[cursor + 2], input[cursor + 3])) {
        tokens.push({ type: TokenType.MultiplyAssign, value: Emojis.Multiply + Emojis.Assign })
        cursor += 4
      } else {
        tokens.push({ type: TokenType.Multiply, value: Emojis.Multiply })
        cursor += 2
      }
    } else if (isMatchEmoji(Emojis.Divide, current, next)) {
      if (isMatchEmoji(Emojis.Assign, next, input[cursor + 2])) {
        tokens.push({ type: TokenType.DivideAssign, value: Emojis.Divide + Emojis.Assign })
        cursor += 3
      } else {
        tokens.push({ type: TokenType.Divide, value: Emojis.Divide })
        cursor += 1
      }
    } else if (isMatchEmoji(Emojis.Increment, current, next)) {
      tokens.push({ type: TokenType.Increment, value: Emojis.Increment })
      cursor += 2
    } else if (isMatchEmoji(Emojis.Decrement, current, next)) {
      tokens.push({ type: TokenType.Decrement, value: Emojis.Decrement })
      cursor += 2
    } else if (NUMBER_REGEX.test(current)) {
      let value = current, isHex = false
      cursor += isMatchEmoji('\uFE0F\u20E3', input[cursor + 1], input[cursor + 2]) ? 3 : 1

      if (input[cursor] === 'x') {
        const next = input[cursor + 1]
        if (value === '0' && next && (NUMBER_REGEX.test(next) || HEX_LETTER.test(next))) {
          value += 'x'
          isHex = true
          cursor++
        } else {
          throwTokenizerError(cursor)
        }
      }

      let cur = input[cursor]
      while (
        (
          NUMBER_REGEX.test(cur) ||
          (isHex ? HEX_LETTER.test(cur) : false) ||
          cur === '_' ||
          cur === '.'
        ) &&
        cursor < length
      ) {
        if (cur === '_') {
          if (NUMBER_REGEX.test(input[cursor + 1]) || (isHex ? HEX_LETTER.test(input[cursor + 1]) : false)) {
            cursor++
          } else {
            throwTokenizerError(cursor)
          }
        } else if (cur === '.') {
          if (isHex || value.includes('.')) {
            throwTokenizerError(cursor)
          } else {
            value += cur
            cursor++
          }
        } else {
          value += cur
          cursor += isMatchEmoji('\uFE0F\u20E3', input[cursor + 1], input[cursor + 2]) ? 3 : 1
        }
        cur = input[cursor]
      }

      tokens.push({ type: TokenType.NumberLiteral, value })
    } else if (VALID_IDENTIFIER_FIRST_CHAR.test(current)) {
      let value = current

      cursor++
      let cur = input[cursor]
      while (VALID_IDENTIFIER.test(cur) && cursor < length) {
        value += cur
        cursor++
        cur = input[cursor]
      }

      tokens.push({ type: TokenType.Identifier, value })
    } else if (current === ' ' || current === '\r') {
      cursor++
    } else if (current === '\n') {
      const last = lastToken(tokens)
      const prev = input[cursor - 1]

      switch (last.type) {
        case TokenType.Dot:
        case TokenType.Comma:
        case TokenType.ParenStart:
        case TokenType.BracketStart:
        case TokenType.CurlyBraceStart:
          break
        default:
          tokens.push({ type: TokenType.Linebreak, value: prev === '\r' ? '\r\n' : '\n' })
          break
      }

      cursor++
    } else if (current === '=') {
      if (next === '=') {
        tokens.push({ type: TokenType.Equal, value: '==' })
        cursor += 2
      } else {
        tokens.push({ type: TokenType.Assign, value: current })
        cursor++
      }
    } else if (current === '.') {
      tokens.push({ type: TokenType.Dot, value: current })
      cursor++
    } else if (current === '(') {
      tokens.push({ type: TokenType.ParenStart, value: current })
      cursor++
    } else if (current === ')') {
      tokens.push({ type: TokenType.ParenEnd, value: current })
      cursor++
    } else if (current === ',') {
      tokens.push({ type: TokenType.Comma, value: current })
      cursor++
    } else if (current === '\'' || current === '"' || current === '`') {
      tokenizeString(current)
    } else if (current === '{') {
      tokens.push({ type: TokenType.CurlyBraceStart, value: current })
      cursor++
    } else if (current === '}') {
      tokens.push({ type: TokenType.CurlyBraceEnd, value: current })
      cursor++
    } else if (current === '[') {
      tokens.push({ type: TokenType.BracketStart, value: current })
      cursor++
    } else if (current === ']') {
      tokens.push({ type: TokenType.BracketEnd, value: current })
      cursor++
    } else if (current === ':') {
      tokens.push({ type: TokenType.Colon, value: current })
      cursor++
    } else if (current === ';') {
      tokens.push({ type: TokenType.Semicolon, value: current })
      cursor++
    } else if (current === '!') {
      if (next === '=') {
        tokens.push({ type: TokenType.NotEqual, value: '!=' })
        cursor += 2
      } else {
        tokens.push({ type: TokenType.Negate, value: current })
        cursor++
      }
    } else if (current === '+') {
      if (next === '+') {
        tokens.push({ type: TokenType.Increment, value: '++' })
        cursor += 2
      } else if (next === '=') {
        tokens.push({ type: TokenType.AddAssign, value: '+=' })
        cursor += 2
      } else {
        tokens.push({ type: TokenType.Add, value: current })
        cursor++
      }
    } else if (current === '-') {
      if (next === '-') {
        tokens.push({ type: TokenType.Decrement, value: '--' })
        cursor += 2
      } else if (next === '=') {
        tokens.push({ type: TokenType.SubstractAssign, value: '-=' })
        cursor += 2
      } else {
        tokens.push({ type: TokenType.Substract, value: current })
        cursor++
      }
    } else if (current === '*') {
      if (next === '=') {
        tokens.push({ type: TokenType.MultiplyAssign, value: '*=' })
        cursor += 2
      } else {
        tokens.push({ type: TokenType.Multiply, value: current })
        cursor++
      }
    } else if (current === '/') {
      if (next === '=') {
        tokens.push({ type: TokenType.DivideAssign, value: '/=' })
        cursor += 2
      } else {
        tokens.push({ type: TokenType.Divide, value: current })
        cursor++
      }
    } else {
      throwTokenizerError(cursor)
    }
  }

  return tokens
}
