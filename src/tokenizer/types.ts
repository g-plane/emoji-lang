export interface Token {
  type: TokenType
  value: string
}

export enum TokenType {
  Identifier,
  StringLiteral,
  NumberLiteral,
  BooleanLiteral,
  Assign,
  QuoteStart,
  QuoteEnd,
  ParenStart,
  ParenEnd,
  BracketStart,
  BracketEnd,
  CurlyBraceStart,
  CurlyBraceEnd,
  Dot,
  Comma,
  Colon,
  Semicolon,
  Add,
  Substract,
  Multiply,
  Divide,
  AddAssign,
  SubstractAssign,
  MultiplyAssign,
  DivideAssign,
  Equal,
  NotEqual,
  Negate,
  Increment,
  Decrement,
  Linebreak,
  CommentPunc,
  Comment,
  IfKeyword,
  ElseKeyword,
}

export enum Emojis {
  Stdout = '📝',
  Comment = '💩',
  True = '✅',
  False = '❌',
  Assign = '🚚',
  Add = '➕',
  Substract = '➖',
  Multiply = '✖️',
  Divide = '➗',
  Equal = '😻',
  NotEqual = '🙀',
  Increment = '📈',
  Decrement = '📉',
  Negate = '✋',
  If = '❓',
  Else = '❗️',
}
