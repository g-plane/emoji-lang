import { Token } from './types'

export function lastToken(tokens: Token[]): Token {
  return tokens[tokens.length - 1]
}

export function throwTokenizerError(position: number): never {
  throw new SyntaxError(`Parsing error at position ${position}`)
}

export function isMatchEmoji(emoji: string, firstChar: string, secondChar: string): boolean {
  /* istanbul ignore else */
  if (emoji.length === 1) {
    return firstChar === emoji[0]
  } else if (emoji.length === 2) {
    return firstChar === emoji[0] && secondChar === emoji[1]
  } else {
    throw new Error(`Too long emoji length: ${emoji}`)
  }
}
