import { tokenizer } from './tokenizer'
import { parser, Program } from './parser'

export function parse(code: string): Program {
  return parser(tokenizer(code))
}
