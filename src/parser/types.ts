export enum NodeType {
  Program = 'Program',
  StringLiteral = 'StringLiteral',
  NumberLiteral = 'NumberLiteral',
  BooleanLiteral = 'BooleanLiteral',
}

export interface Node {
  type: NodeType
}

export interface Program extends Node {
  type: NodeType.Program
  body: Node[]
}

export interface StringLiteral extends Node {
  type: NodeType.StringLiteral,
  value: string,
  raw: string
}

export interface NumberLiteral extends Node {
  type: NodeType.NumberLiteral,
  value: number,
  raw: string
}

export interface BooleanLiteral extends Node {
  type: NodeType.BooleanLiteral,
  value: boolean,
  raw: string
}
