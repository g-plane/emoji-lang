import { Token, TokenType as tt } from '../tokenizer/types'
import * as t from './types'
import * as utils from './utils'

export interface Program {
  ast: t.Node
}

export function parser(tokens: Token[]): Program {
  const { length } = tokens
  let cursor = 0
  let current = tokens[cursor]
  let next = tokens[cursor + 1]

  const program: t.Program = { type: t.NodeType.Program, body: [] }
  const result: Program = { ast: program }

  function parseNumberLiteral(): t.NumberLiteral {
    cursor++
    return {
      type: t.NodeType.NumberLiteral,
      value: current.value.includes('.') ? parseFloat(current.value) : parseInt(current.value), // TODO
      raw: current.value
    }
  }

  function parseStringLiteral(): t.StringLiteral {
    const value = tokens[cursor + 1].value
    const raw = `${current.value}${value}${tokens[cursor + 2].value}`
    cursor += 3
    return {
      type: t.NodeType.StringLiteral,
      value,
      raw
    } as t.StringLiteral
  }

  function parseMaybeMemberExpression(startNode: Token) {
    if (tokens[cursor + 2].type === tt.Identifier) {
      cursor += 2
      return {
        //
      }
    }
  }

  function parseCallExpression() {
    //
  }

  function walk(): t.Node {
    current = tokens[cursor]
    next = tokens[cursor + 1]
    switch (current.type) {
      case tt.NumberLiteral:
        return parseNumberLiteral()
      case tt.QuoteStart:
        return parseStringLiteral()
      case tt.Identifier:
        //
      default:
        return utils.throwParserError()
    }
  }

  while (cursor < length) {
    program.body.push(walk())
  }

  return result
}
