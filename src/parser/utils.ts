export function throwParserError(): never {
  throw new SyntaxError('Parser Error')
}
